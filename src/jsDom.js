let form = document.getElementById("form")

let itemsList = document.getElementById("collection")

let selectAll = document.getElementById("selectAll")

let footer = document.getElementById("footer")

let count = document.getElementById("count")

let todo_container = document.getElementById("todo_container")

let countAll = itemsList.children.length

selectAll.classList.add("hidden")

selectAll.addEventListener('click', selectAllLists)

let allButton = document.querySelector("#listButtons button:nth-child(1)")
let activeButton = document.querySelector("#listButtons button:nth-child(2)")
let completedButton = document.querySelector("#listButtons button:nth-child(3)")
let clearButton = document.getElementById("clearButton")

function additem(e) {

    e.preventDefault()
    // console.log("hii i am coming")
    let newItem = document.getElementById("inputText").value.trim()

    if (newItem !== "") {

        let li = document.createElement('li')
        li.className = "p-6 mx-5 border-b-2 border-grey w-full"
        let checkbox = document.createElement('input')
        checkbox.className = "mr-5"
        checkbox.setAttribute('type', 'checkbox')

        let label = document.createElement("label")
        label.textContent = newItem
        let button = document.createElement('button')
        button.className = "text-red-800 font-extrabold float-end mr-4 opacity-0"
        button.appendChild(document.createTextNode('X'))

        li.appendChild(checkbox)
        li.appendChild(label)
        li.appendChild(button)

        itemsList.appendChild(li)
        selectAll.classList.remove("hidden")
        countAll+=1;
        
        footer.classList.remove("hidden")
        countUpadte()
    }

    document.getElementById("inputText").value = "";
}

form.addEventListener('submit', additem) // function call for the add items into the list

function removeItem(event) {

    let li = event.target.parentNode
    let label = li.querySelector('label')
    let checkbox = li.querySelector('input')

    if (event.target.tagName === "BUTTON") {
        // console.log(event.target.parentElement)
        let removedOne = event.target.parentElement
        countAll -= 1
        itemsList.removeChild(removedOne)

        if (itemsList.children.length == 0){

            footer.classList.add("hidden")
            selectAll.style.display = "none"

        }
    }

    else if (event.target.tagName === "INPUT") {

        if (checkbox.checked) {
            label.style.textDecoration = 'line-through';
            countAll -= 1;
        }

        else {
            label.style.textDecoration = 'none';
            countAll += 1;
        }
    }
    countUpadte()

}

itemsList.addEventListener('click', removeItem) // function to remove the items from the list and to linethorught the label by checking the checkbox

itemsList.addEventListener('dblclick', function (event) {
    //let listItem = event.target.closest('li')
    let listItem = event.target.parentNode
    
    if (listItem) {
        editItem(listItem)

    }
})

function editItem(listItem) {

    const label = listItem.querySelector('label')
    const checkbox = listItem.querySelector('input')
    const button = listItem.querySelector('button')
    // console.log(button)
    let currentText = label.textContent
    let input = document.createElement('input')
    input.type = "text"
    input.value = currentText
    // console.log(label.parentNode,"hiii")
    label.parentNode.replaceChild(input, label)

    input.focus()
    input.className = 'p-6 w-full'
    checkbox.classList.add('hidden')
    button.classList.add('hidden')

    input.addEventListener('keypress', function (event) {

        if (event.key === "Enter") {
            label.textContent = input.value

            input.parentNode.replaceChild(label, input)
            checkbox.classList.remove('hidden')
            button.classList.remove('hidden')

        }
    })


    input.addEventListener('blur', function () {

        label.textContent = label.textContent
        input.parentNode.replaceChild(label, input)
        checkbox.classList.remove('hidden')
        button.classList.remove('hidden')

    })

}


itemsList.addEventListener('mouseover', function (event) {

    let listOver = event.target.closest('li')

    if (listOver) {
        let button = listOver.querySelector('button')
        button.classList.remove('opacity-0')
        button.classList.add('opacity-1')
    }

})

itemsList.addEventListener('mouseout', function (event) {

    let listOver = event.target
    // console.log(event.target,"hii")
    
    if (listOver && event.target.tagName === "LI") {
        let button = listOver.querySelector('button')
        button.classList.remove('opacity-1')
        button.classList.add('opacity-0')
    }
})


function selectAllLists() {

    console.log("hiiii")
    let childernElements = itemsList.children


    if (selectAll.checked) {

        for (let child = 0; child < childernElements.length; child++) {
            //console.log(childernElements[child])

            let checkbox = childernElements[child].querySelector('input')
            let label = childernElements[child].querySelector('label')


            checkbox.checked = true
            label.style.textDecoration = 'line-through'
        }

        countAll = 0
        countUpadte()
        
    }
    else {

        for (let child = 0; child < childernElements.length; child++) {
            //console.log(childernElements[child])

            let checkbox = childernElements[child].querySelector('input')
            let label = childernElements[child].querySelector('label')


            checkbox.checked = false
            label.style.textDecoration = 'none'
            
        }

        countAll = itemsList.children.length
        countUpadte()
    }
    
    // console.log(itemsList.children)
    console.log(childernElements.length)
}

function countUpadte(){
    
    if (countAll > 0){
        count.textContent = `${countAll} items left!`


    }else{
        count.textContent = '0 items left!'
    }

}

function showAllItems(){
    let childernElements = itemsList.children
        for (let child = 0; child < childernElements.length; child++) {
            let checkbox = childernElements[child].querySelector('input')
            childernElements[child].style.display = "block"
            
        }
}

function showActiveItems(){
     
        let childernElements = itemsList.children
        for (let child = 0; child < childernElements.length; child++) {
            

            let checkbox = childernElements[child].querySelector('input')
            
            if (!checkbox.checked){
                childernElements[child].style.display = "block"
            }
            else{
                childernElements[child].style.display = "none"
            }
        }
}

function showCompletedItems(){

    let childernElements = itemsList.children
        for (let child = 0; child < childernElements.length; child++) {
            

            let checkbox = childernElements[child].querySelector('input')
            
            if (checkbox.checked){
                childernElements[child].style.display = "block"
            }
            else{
                childernElements[child].style.display = "none"
            }
        }
}

function clearItemsLi(){

    let childernElements = itemsList.children
        for (let child = childernElements.length-1;child >=0; child--) {

            let checkbox = childernElements[child].querySelector('input')

            if (checkbox.checked){
                //itemsList.removeChild(childernElements[child])
                childernElements[child].remove()
                if (itemsList.children.length == 0){

                    footer.classList.add("hidden")
                    selectAll.style.display = "none"
        
                }
            }
            
        }
}

allButton.addEventListener('click',showAllItems) // this will call function to show all the list items
activeButton.addEventListener('click',showActiveItems) // this function will call only the active items that are not checked in checkbox
completedButton.addEventListener('click',showCompletedItems) // this function will call only the checked checkboxes
clearButton.addEventListener('click',clearItemsLi) // this function will clear the check items in teh list















